import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenInitiativeComponent } from './green-initiative.component';

describe('GreenInitiativeComponent', () => {
  let component: GreenInitiativeComponent;
  let fixture: ComponentFixture<GreenInitiativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreenInitiativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreenInitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
