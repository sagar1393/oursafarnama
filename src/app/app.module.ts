import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Shared/Component/header/header.component';
import { LoginComponent } from './Component/login/login.component';
import { HomeComponent } from './Component/home/home.component';
import { FooterComponent } from './Shared/Component/footer/footer.component';
import { AboutUsComponent } from './Component/about-us/about-us.component';
import { GalleryComponent } from './Component/gallery/gallery.component';
import { GreenInitiativeComponent } from './Component/green-initiative/green-initiative.component';
import { TripsComponent } from './Component/trips/trips.component';
import { TreksComponent } from './Component/trips/treks/treks.component';
import { BackpackingComponent } from './Component/trips/backpacking/backpacking.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    FooterComponent,
    AboutUsComponent,
    GalleryComponent,
    GreenInitiativeComponent,
    TripsComponent,
    TreksComponent,
    BackpackingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
