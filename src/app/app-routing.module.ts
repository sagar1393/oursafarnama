import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './Component/home/home.component';
import {AboutUsComponent} from './Component/about-us/about-us.component';
import {GalleryComponent} from './Component/gallery/gallery.component';
import {GreenInitiativeComponent} from './Component/green-initiative/green-initiative.component';
import {TripsComponent} from './Component/trips/trips.component';
import {TreksComponent} from './Component/trips/treks/treks.component';
import {BackpackingComponent} from './Component/trips/backpacking/backpacking.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent},
  {path: 'gallery', component: GalleryComponent},
  {path: 'green-initiative', component: GreenInitiativeComponent},
  {path: 'trips', component: TripsComponent},
  {path: 'treks', component: TreksComponent},
  {path: 'backpacking', component: BackpackingComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
